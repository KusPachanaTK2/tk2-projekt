package pl.edu.agh.tk

import scala.util.matching.Regex
import scala.util.parsing.combinator._

object LogExpParser extends RegexParsers {
  //*
  def word: Parser[String] = """\w+""".r ^^ {
    _ toString
  }

  def opera: Parser[String] = "AND" | "OR" ^^ {
    _ toString
  }

  def operaNot: Parser[String] = "NOT" ^^ {
    _ toString
  }
  
  def operaComplex: Parser[String] = "=>" | "<=>" ^^ {
    _ toString
  }

  def expression: Parser[String] = expressionOperaComplex | expressionNoOperaComplex ^^ {
    _ toString
  }

  def expressionNoOperaComplex: Parser[String] = simpleExpression | notSoComplexExpression | complexExpression | word ^^ {
    _ toString
  }


  def expressionOperaComplex: Parser[String] = expressionOperaComplexToParse | expressionOperaComplexNoBracketsToParse  ^^ {
    _ toString
  }

  def expressionOperaComplexToParse: Parser[String] = "(" ~> expressionNoOperaComplex ~ operaComplex ~ expressionNoOperaComplex <~ ")" ^^ {
    case left ~ "=>" ~ right => {
      left + " => " + right
    }
    case left ~ "<=>" ~ right => {
      left + " <=> " + right
    }
  }

  def expressionOperaComplexNoBracketsToParse: Parser[String] = expressionNoOperaComplex ~ operaComplex ~ expressionNoOperaComplex ^^ {
    case left ~ "=>" ~ right => {
      left + " => " + right
    }
    case left ~ "<=>" ~ right => {
      left + " <=> " + right
    }
  }

  def deMorganExpression: Parser[String] = "(" ~> "(" ~> operaNot ~ word ~ ")" ~ opera ~ "(" ~ operaNot ~ word <~ ")" <~ ")" ^^ {
    case "NOT" ~ left ~ ")" ~ "AND" ~ "(" ~ "NOT" ~ right => {
      "NOT " + "(" + left + " OR " + right + ")"
    }
    case "NOT" ~ left ~ ")" ~ "OR" ~ "(" ~ "NOT" ~ right => {
      "NOT " + "(" + left + " AND " + right + ")"
    }
  }

  def deMorganExpressionNoBrackets: Parser[String] = operaNot ~ word ~ opera ~ operaNot ~ word ^^ {
    case "NOT" ~ left ~ "AND" ~ "NOT" ~ right => {
      "NOT " + "(" + left + " OR " + right + ")"
    }
    case "NOT" ~ left ~ "OR" ~ "NOT" ~ right => {
      "NOT " + "(" + left + " AND " + right + ")"
    }
  }

  def deMorganExpressionLessBrackets: Parser[String] = "(" ~> operaNot ~ word ~ opera ~ operaNot ~ word <~ ")" ^^ {
    case "NOT" ~ left ~ "AND" ~ "NOT" ~ right => {
      "NOT " + "(" + left + " OR " + right + ")"
    }
    case "NOT" ~ left ~ "OR" ~ "NOT" ~ right => {
      "NOT " + "(" + left + " AND " + right + ")"
    }
  }

  def expressionNot: Parser[String] = "(" ~> operaNot ~ "(" ~ expression <~ ")" <~ ")" ^^ {
    case "NOT" ~ "(" ~ expression => {
      "NOT " + "(" + expression + ")"
    }
  }

  def expressionNotLessBrackets: Parser[String] = operaNot ~ "(" ~ expression <~ ")" ^^ {
    case "NOT" ~ "(" ~ expression => {
      "NOT " + "(" + expression + ")"
    }
  }

  def simpleExpressionDoubleNot: Parser[String] = "(" ~> operaNot ~ "(" ~ operaNot ~ simpleExpression <~ ")" <~ ")" ^^ {
    case "NOT" ~ "(" ~ "NOT" ~ expression => {
      expression
    }
  }

  def simpleExpressionDoubleNotNoBrackets: Parser[String] = operaNot ~  operaNot ~ simpleExpression ^^ {
    case "NOT" ~ "NOT" ~ expression => {
      expression
    }
  }

  def simpleExpressionDoubleNotLessBrackets: Parser[String] = "(" ~> operaNot ~ operaNot ~ simpleExpression <~ ")" ^^ {
    case "NOT" ~ "NOT" ~ expression => {
      expression
    }
  }

  def notSoComplexExpression: Parser[String] = notSoComplexExpressionLeft | notSoComplexExpressionRight ^^ {
    _ toString
  }

  def notSoComplexExpressionLeft: Parser[String] = notSoComplexExpressionLeftToParse ^^ {
    _ toString
  }

  def notSoComplexExpressionLeftToParse: Parser[String] = "(" ~> word ~ opera ~ expression <~ ")" ^^ {
    case left ~ "AND" ~ right => {
      if (left equals "FALSE") {
        "FALSE"
      } else if (left equals "TRUE") {
        right
      } else {
        "(" + left + " AND " + right + ")"
      }
    }
    case left ~ "OR" ~ right => {
      if (left equals "FALSE") {
        right
      } else if (left equals "TRUE") {
        "TRUE"
      } else {
        "(" + left + " OR " + right + ")"
      }
    }
  }

  def notSoComplexExpressionRight: Parser[String] = notSoComplexExpressionRightToParse ^^ {
    _ toString
  }

  def notSoComplexExpressionRightToParse: Parser[String] = "(" ~> expression ~ opera ~ word <~ ")" ^^ {
    case left ~ "AND" ~ right => {
      if (right equals "FALSE") {
        "FALSE"
      } else if (right equals "TRUE") {
        left
      } else {
        "(" + left + " AND " + right + ")"
      }
    }
    case left ~ "OR" ~ right => {
      if (right equals "FALSE") {
        left
      } else if (right equals "TRUE") {
        "TRUE"
      } else {
        "(" + left + " OR " + right + ")"
      }
    }
  }

  def complexExpression: Parser[String] = complexExpressionToParse ^^ {
    _ toString
  }

  def complexExpressionToParse: Parser[String] = "(" ~>  expression ~ opera ~ expression <~ ")" ^^ {
    case left ~ "AND" ~ right => {
      "(" + left + " AND " + right + ")"
    }
    case left ~ "OR" ~ right => {
      "(" + left + " OR " + right + ")"
    }
  }

  def moreComplexExpressionToParse: Parser[String] = expression ~ opera ~ expression ^^ {
    case left ~ "AND" ~ right => {
      left + " AND " + right
    }
    case left ~ "OR" ~ right => {
      left + " OR " + right
    }
  }

  def simpleExpression: Parser[String] = simpleExpressionDoubleNot | simpleExpressionDoubleNotLessBrackets | simpleExpressionDoubleNotNoBrackets |
                        deMorganExpression | deMorganExpressionLessBrackets | deMorganExpressionNoBrackets | expressionNot | expressionNotLessBrackets |
                        simpleExpressionToParseWithLeftNot | simpleExpressionToParseWithLeftNotNoBrackets | simpleExpressionToParseWithLeftNotExtraBrackets |
		  									simpleExpressionToParseWithRightNot | simpleExpressionToParseWithRightNotNoBrackets | simpleExpressionToParseWithRightNotExtraBrackets |
                        wordWithDoubleNot | wordWithDoubleNotLessBrackets | wordWithDoubleNotNoBrackets | wordWithNot | wordWithNotNoBrackets |
		  									simpleExpressionToParse | simpleExpressionToParseNoBrackets ^^ {
    _ toString
  }

  def simpleExpressionToParse: Parser[String] = "(" ~> word ~ opera ~ word  <~ ")" ^^ {
    case left ~ "AND" ~ right => {
      if (left equals right) {
        left
      } else if (left equals "TRUE") {
        right
      } else if (right equals "TRUE") {
        left
      } else if ((left equals "FALSE") | (right equals "FALSE")) {
        "FALSE"
      } else {
        "(" + left + " AND " + right + ")"
      }
    }
    case left ~ "OR" ~ right => {
      if (left equals right) {
        left
      } else if ((left equals "TRUE") | (right equals "TRUE")) {
        "TRUE"
      } else if (left equals "FALSE") {
        right
      } else if (right equals "FALSE") {
        left
      } else {
        "(" + left + " OR " + right + ")"
      }
    }
  }
  
  def simpleExpressionToParseNoBrackets: Parser[String] = word ~ opera ~ word ^^ {
    case left ~ "AND" ~ right => {
      if (left equals right) {
        left
      } else if (left equals "TRUE") {
        right
      } else if (right equals "TRUE") {
        left
      } else if ((left equals "FALSE") | (right equals "FALSE")) {
        "FALSE"
      } else {
        left + " AND " + right
      }
    }
    case left ~ "OR" ~ right => {
      if (left equals right) {
        left
      } else if ((left equals "TRUE") | (right equals "TRUE")) {
        "TRUE"
      } else if (left equals "FALSE") {
        right
      } else if (right equals "FALSE") {
        left
      } else {
        left + " OR " + right
      }
    }
  }

  def wordWithDoubleNot: Parser[String] = "(" ~> "(" ~> operaNot ~ "(" ~ operaNot  ~ word <~ ")" <~ ")"  ^^ {
    case "NOT" ~ "(" ~ "NOT" ~ word => {
      word
    }
  }

  def wordWithDoubleNotNoBrackets: Parser[String] = operaNot ~ operaNot  ~ word^^ {
    case "NOT" ~ "NOT" ~ word => {
      word
    }
  }

  def wordWithDoubleNotLessBrackets: Parser[String] = operaNot ~ "(" ~ operaNot  ~ word <~ ")" ^^ {
    case "NOT" ~ "(" ~ "NOT" ~ word => {
      word
    }
  }

  def wordWithNot: Parser[String] = "(" ~> operaNot  ~ word <~ ")"  ^^ {
    case "NOT" ~ word => {
      if (word equals "TRUE") {
        "FALSE"
      } else if (word equals "FALSE") {
        "TRUE"
      } else {
        "(NOT " + word + ")"
      }
    }
  }
  
  def wordWithNotNoBrackets: Parser[String] = operaNot  ~ word  ^^ {
    case "NOT" ~ word => {
      if (word equals "TRUE") {
        "FALSE"
      } else if (word equals "FALSE") {
        "TRUE"
      } else {
        "NOT " + word
      }
    }
  }
  
   def simpleExpressionToParseWithLeftNot: Parser[String] = "(" ~> operaNot ~ word ~ opera ~ word <~ ")" ^^ {
    case "NOT" ~ left ~ "AND" ~ right => {
      if (left equals right) {
        "FALSE"
      } else if (right equals "TRUE") {
        "(NOT " + left + ")"
      } else if (right equals "FALSE") {
        "FALSE"
      } else if(left equals "TRUE"){
        "FALSE"
      } else if(left equals "FALSE"){
        right
      } else {
        "(NOT " + left + " AND " + right + ")"
      }
    }
    case "NOT" ~ left ~ "OR" ~ right => {
      if (left equals right) {
        "TRUE"
      } else if (right equals "TRUE") {
        "TRUE"
      } else if (right equals "FALSE") {
        "(NOT " + left + ")"
      } else if(left equals "TRUE"){
        right
      } else if(left equals "FALSE"){
        "TRUE"
      } else {
        "(NOT " + left + " OR " + right + ")"
      }
    }
  }

  def simpleExpressionToParseWithLeftNotExtraBrackets: Parser[String] = "(" ~> "(" ~> operaNot ~ word ~ ")" ~ opera ~ word <~ ")" ^^ {
    case "NOT" ~ left ~ ")" ~ "AND" ~ right => {
      if (left equals right) {
        "FALSE"
      } else if (right equals "TRUE") {
        "(NOT " + left + ")"
      } else if (right equals "FALSE") {
        "FALSE"
      } else if(left equals "TRUE"){
        "FALSE"
      } else if(left equals "FALSE"){
        right
      } else {
        "(NOT " + left + ") AND " + right + ")"
      }
    }
    case "NOT" ~ left ~ ")" ~ "OR" ~ right => {
      if (left equals right) {
        "TRUE"
      } else if (right equals "TRUE") {
        "TRUE"
      } else if (right equals "FALSE") {
        "(NOT " + left + ")"
      } else if(left equals "TRUE"){
        right
      } else if(left equals "FALSE"){
        "TRUE"
      } else {
        "(NOT " + left + ") OR " + right + ")"
      }
    }
  }

   def simpleExpressionToParseWithLeftNotNoBrackets: Parser[String] = operaNot ~ word ~ opera ~ word ^^ {
    case "NOT" ~ left ~ "AND" ~ right => {
      if (left equals right) {
        "FALSE"
      } else if (right equals "TRUE") {
        "NOT " + left
      } else if (right equals "FALSE") {
        "FALSE"
      } else if(left equals "TRUE"){
        "FALSE"
      } else if(left equals "FALSE"){
        right
      } else {
        "NOT " + left + " AND " + right
      }
    }
    case "NOT" ~ left ~ "OR" ~ right => {
      if (left equals right) {
        "TRUE"
      } else if (right equals "TRUE") {
        "TRUE"
      } else if (right equals "FALSE") {
        "NOT " + left
      } else if(left equals "TRUE"){
        right
      } else if(left equals "FALSE"){
        "TRUE"
      } else {
        "NOT " + left + " OR " + right
      }
    }
  }

   def simpleExpressionToParseWithRightNot: Parser[String] = "(" ~> word ~ opera ~ operaNot ~ word <~ ")" ^^ {
    case left ~ "AND" ~ "NOT" ~  right => {
      if (left equals right) {
        "FALSE"
      } else if (left equals "TRUE") {
        "(NOT " + right + ")"
      } else if (left equals "FALSE") {
        "FALSE"
      } else if(right equals "TRUE"){
        "FALSE"
      } else if(right equals "FALSE"){
        right
      } else {
        "(" + left + " AND NOT " + right + ")"
      }
    }
    case "NOT" ~ left ~ "OR" ~ right => {
      if (left equals right) {
        "TRUE"
      } else if (left equals "TRUE") {
        "TRUE"
      } else if (left equals "FALSE") {
        "(NOT " + right + ")"
      } else if(right equals "TRUE"){
        left
      } else if(right equals "FALSE"){
        "TRUE"
      } else {
        "(" + left + " OR NOT" + right + ")"
      }
    }
  }

  def simpleExpressionToParseWithRightNotExtraBrackets: Parser[String] = "(" ~> word ~ opera ~ "(" ~ operaNot ~ word <~ ")" <~ ")" ^^ {
    case left ~ "AND" ~ "NOT" ~ "(" ~ right => {
      if (left equals right) {
        "FALSE"
      } else if (left equals "TRUE") {
        "(NOT " + right + ")"
      } else if (left equals "FALSE") {
        "FALSE"
      } else if(right equals "TRUE"){
        "FALSE"
      } else if(right equals "FALSE"){
        right
      } else {
        "(" + left + " AND (NOT " + right + "))"
      }
    }
    case "NOT" ~ left ~ "OR" ~ "(" ~ right => {
      if (left equals right) {
        "TRUE"
      } else if (left equals "TRUE") {
        "TRUE"
      } else if (left equals "FALSE") {
        "(NOT " + right + ")"
      } else if(right equals "TRUE"){
        left
      } else if(right equals "FALSE"){
        "TRUE"
      } else {
        "(" + left + " OR (NOT " + right + "))"
      }
    }
  }

   def simpleExpressionToParseWithRightNotNoBrackets: Parser[String] = word ~ opera ~ operaNot ~  word ^^ {
    case left ~ "AND" ~ "NOT" ~  right => {
      if (left equals right) {
        "FALSE"
      } else if (left equals "TRUE") {
        "NOT " + right
      } else if (left equals "FALSE") {
        "FALSE"
      } else if(right equals "TRUE"){
        "FALSE"
      } else if(right equals "FALSE"){
        left
      } else {
        left + " AND NOT " +  right
      }
    }
    case left ~ "OR" ~ "NOT" ~ right => {
      if (left equals right) {
        "TRUE"
      } else if (right equals "TRUE") {
        "TRUE"
      } else if (right equals "FALSE") {
        "NOT " + left
      } else if(left equals "TRUE"){
        right
      } else if(left equals "FALSE"){
        "TRUE"
      } else {
        left + " OR NOT " +  right
      }
    }
  }

   
  // apply function
  def apply(input: String): Option[String] = parseAll(expression, input) match {
    case Success(result, _) => Some(result)
    case NoSuccess(_, _) => None
  }
  //	*/
  /*
      def expression = andExp


    // AND expressions
    def andExp: Parser[String] = falseAndExp | trueAndExp | basicAndExp ^^ { _ toString }
    // "false" returning AND expressions
    def falseAndExp: Parser[String] = boolFirstFalseAndExp | boolLastFalseAndExp ^^ { _ toString }
    def boolFirstFalseAndExp: Parser[String] = falseVal ~ andOp ~ value ^^ { case falsevalue~and~somevalue => falsevalue }
    def boolLastFalseAndExp: Parser[String] = (trueVal | charVal) ~ andOp ~ falseVal ^^ { case somevalue~and~falsevalue => falsevalue }
    // "true" returning AND expressions
    def trueAndExp: Parser[String] = boolLastTrueAndExp | charLastTrueAndExp ^^ { _ toString }
    def boolLastTrueAndExp: Parser[String] = value ~ andOp ~ trueVal ^^ { case somevalue~and~truevalue => truevalue }
    def charLastTrueAndExp: Parser[String] = trueVal ~ andOp ~ charVal ^^ { case truevalue~and~charvalue => charvalue }
    // basic AND expression
    def basicAndExp: Parser[String] = charVal~andOp~charVal ^^ {
      case leftVal~and~rightVal => {
        if(leftVal equals rightVal)
          leftVal
        else
          leftVal+" and "+rightVal
      }
    }

    // basic definitions
    def operator: Parser[String] = andOp | orOp ^^ { _ toString }
    def andOp: Parser[String] = "and" | "AND" ^^ { case _ => "and" }
    def orOp: Parser[String] = "or" | "OR" ^^ { case _ => "or" }
    def value: Parser[String] = trueVal | falseVal | charVal ^^ { _ toString }
    def charVal: Parser[String] = """\w+""".r ^^ { _ toString }
    def trueVal: Parser[String] = trueLogicVal | trueBitVal ^^ { _ toString }
    def trueLogicVal: Parser[String] = "true" | "TRUE" ^^ { case _ => "true" }
    def trueBitVal: Parser[String] = "1" ^^ { _ toString }
    def falseVal: Parser[String] = falseLogicVal | falseBitVal ^^ { _ toString }
    def falseLogicVal: Parser[String] = "false" | "FALSE" ^^ { case _ => "false" }
    def falseBitVal: Parser[String] = "0" ^^ { _ toString }

    // apply function
    def apply(input: String): Option[String] = parseAll(expression, input) match {
        case Success(result, _) => Some(result)
        case NoSuccess(_, _) => None
    }
    */
}