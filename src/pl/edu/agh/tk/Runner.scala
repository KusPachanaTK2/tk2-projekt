package pl.edu.agh.tk

object Runner {
  def main(args: Array[String]) {

    println("Welcome to Logic Expressions Interpeter. Type QUIT to exit.")
    print("LogExp>> ")
    var input = readLine()

    while (!(input.equals("QUIT") || input.equals("quit"))) {
      println(parse(input.toUpperCase()))
      print("LogExp>> ")
      input = readLine()
    }
  }

  def parse(inputToParse: String): String = {
    var tmp = inputToParse.toUpperCase()
    var old = ""
    while (!(tmp equals old)) {
      old = tmp
      LogExpParser(tmp) match {
        case Some(result) => tmp = result
        case None => tmp = "Not a valid logic expresion."
      }
    }
    return tmp
  }
  
}