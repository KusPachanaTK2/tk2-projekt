package junit

import org.junit.Test;
import org.junit.Assert._;
import pl.edu.agh.tk.Runner;

class ExpTest {
  
  @Test def testNr1() {
    assertEquals("Powinno byc: A", "A", Runner.parse("(a and a)".toUpperCase()))
  }
  
  @Test def testNr2() {
    assertEquals("Powinno byc: (A AND B)", "(A AND B)", Runner.parse("(a and b)".toUpperCase()))
  }
  
  @Test def testNr3() {
    assertEquals("Powinno byc: (A OR B)", "(A OR B)", Runner.parse("(a or b)".toUpperCase()))
  }
  
  @Test def testNr4() {
    assertEquals("Powinno byc: A", "A", Runner.parse("((a and a) or a)".toUpperCase()))
  }
  
  @Test def testNr5() {
    assertEquals("Powinno byc: (A OR B)", "(A OR B)", Runner.parse("((a and a) or b)".toUpperCase()))
  }
  
  @Test def testNr6() {
    assertEquals("Powinno byc: A", "A", Runner.parse("(true and (a and a))".toUpperCase()))
  }
  
  @Test def testNr7() {
    assertEquals("Powinno byc: TRUE", "TRUE", Runner.parse("(true or (a and a))".toUpperCase()))
  }
  
  @Test def testNr8() {
    assertEquals("Powinno byc: (A AND B)", "(A AND B)", Runner.parse("(true and (a and b))".toUpperCase()))
  }
  
  @Test def testNr9() {
    assertEquals("Powinno byc: TRUE", "TRUE", Runner.parse("(true or (a and b))".toUpperCase()))
  }
  
  @Test def testNr10() {
    assertEquals("Powinno byc: (A AND B)", "(A AND B)", Runner.parse("((a and b) and true)".toUpperCase()))
  }
  
  @Test def testNr11() {
    assertEquals("Powinno byc: TRUE", "TRUE", Runner.parse("((a and b) or true)".toUpperCase()))
  }
  
  @Test def testNr12() {
    assertEquals("Powinno byc: FALSE", "FALSE", Runner.parse("(false and (a and a))".toUpperCase()))
  }
  
  @Test def testNr13() {
    assertEquals("Powinno byc: A", "A", Runner.parse("(false or (a and a))".toUpperCase()))
  }
  
  @Test def testNr14() {
    assertEquals("Powinno byc: FALSE", "FALSE", Runner.parse("(false and (a and b))".toUpperCase()))
  }
  
  @Test def testNr15() {
    assertEquals("Powinno byc: (A AND B)", "(A AND B)", Runner.parse("(false or (a and b))".toUpperCase()))
  }
  
  @Test def testNr16() {
    assertEquals("Powinno byc: FALSE", "FALSE", Runner.parse("((a and b) and false)".toUpperCase()))
  }
  
  @Test def testNr17() {
    assertEquals("Powinno byc: (A AND B)", "(A AND B)", Runner.parse("((a and b) or false)".toUpperCase()))
  }
  
  @Test def testNr18() {
    assertEquals("Powinno byc: A", "A", Runner.parse("((a and a) and true)".toUpperCase()))
  }
  
  @Test def testNr19() {
    assertEquals("Powinno byc: TRUE", "TRUE", Runner.parse("((a and a) or true)".toUpperCase()))
  }
    
  @Test def testNr20() {
    assertEquals("Powinno byc: TRUE", "TRUE", Runner.parse("((a and a) or (b or true))".toUpperCase()))
  }
  
  @Test def testNr21() {
    assertEquals("Powinno byc: TRUE", "TRUE", Runner.parse("((b or true) or (a and a))".toUpperCase()))
  }
  
  @Test def testNr22() {
    assertEquals("Powinno byc: A", "A", Runner.parse("((a and a) and (b or true))".toUpperCase()))
  }
  
  @Test def testNr23() {
    assertEquals("Powinno byc: A", "A", Runner.parse("((b or true) and (a and a))".toUpperCase()))
  }
  
  @Test def testNr24() {
    assertEquals("Powinno byc: A", "A", Runner.parse("(((a and a) and (b or true)) or a)".toUpperCase()))
  }
  
  @Test def testNr25() {
    assertEquals("Powinno byc: (C OR A)", "(C OR A)", Runner.parse("((false and (((a and a) and (b or true)) or a)) or (c or (true and a)))".toUpperCase()))
  }
  
  @Test def testNr26() {
    assertEquals("Powinno byc: ((D AND A) or (E OR C))", "((D AND A) OR (E OR C))", Runner.parse("((d and (((a and a) and (b or true)) or a)) or ((true and e) or c))".toUpperCase()))
  }
  
  @Test def testNr27() {
    assertEquals("Powinno byc: ((D AND A) OR (E OR (C AND A)))", "((D AND A) OR (E OR (C AND A)))", Runner.parse("((d and (((a and a) and (b or true)) or a)) or ((true and e) or (c and a)))".toUpperCase()))
  }
  
  @Test def testNr28() {
    assertEquals("Powinno byc: FALSE", "FALSE", Runner.parse("(not a and a)".toUpperCase()))
  }

  @Test def testNr29() {
    assertEquals("Powinno byc: A", "A", Runner.parse("not not a and a".toUpperCase()))
  }

  @Test def testNr30() {
    assertEquals("Powinno byc: NOT (A OR B)", "NOT (A OR B)", Runner.parse("not (a or b)".toUpperCase()))
  }

  @Test def testNr31() {
    assertEquals("Powinno byc: NOT (A OR B)", "NOT (A OR B)", Runner.parse("not a and not b".toUpperCase()))
  }

  @Test def testNr32() {
    assertEquals("Powinno byc: NOT (A AND B)", "NOT (A AND B)", Runner.parse("not a or not b".toUpperCase()))
  }

  @Test def testNr33() {
    assertEquals("Powinno byc: (A OR B)", "(A OR B)", Runner.parse("NOT NOT (A OR B)".toUpperCase()))
  }

  @Test def testNr34() {
    assertEquals("Powinno byc: A", "A", Runner.parse("NOT NOT A".toUpperCase()))
  }

  @Test def testNr35() {
    assertEquals("Powinno byc: TRUE", "TRUE", Runner.parse("NOT A OR A".toUpperCase()))
  }

  @Test def testNr36() {
    assertEquals("Powinno byc: A OR NOT B", "A OR NOT B", Runner.parse("NOT NOT A OR NOT B".toUpperCase()))
  }

  @Test def testNr37() {
    assertEquals("Powinno byc: A AND NOT B", "A AND NOT B", Runner.parse("NOT NOT A AND NOT B".toUpperCase()))
  }

  @Test def testNr38() {
    assertEquals("Powinno byc: NOT (A AND B)", "NOT (A AND B)", Runner.parse("((NOT A) OR (NOT B))".toUpperCase()))
  }

  @Test def testNr39() {
    assertEquals("Powinno byc: A AND B AND C AND D", "(A AND B AND C AND D)", Runner.parse("(A AND B AND C AND D)".toUpperCase()))
  }

  @Test def testNr40() {
    assertEquals("Powinno byc: (NOT (A OR B) OR D)", "(NOT (A OR B) OR D)", Runner.parse("((NOT A AND NOT B) OR (D AND TRUE))".toUpperCase()))
  }

  @Test def testNr41() {
    assertEquals("Powinno byc: A OR B", "A OR NOT B => (A OR C)", Runner.parse("(NOT NOT A OR NOT B) => ((A AND A) OR C)".toUpperCase()))
  }

  @Test def testNr42() {
    assertEquals("Powinno byc: A => (A AND B)", "A => (A AND B)", Runner.parse("(((a and a) and (b or true)) or a) => (A AND B)".toUpperCase()))
  }

  @Test def testNr43() {
    assertEquals("Powinno byc: A <=> (A AND B)", "A <=> (A AND B)", Runner.parse("(((a and a) and (b or true)) or a) <=> (A AND B)".toUpperCase()))
  }

  @Test def testNr44() {
    assertEquals("Powinno byc: A <=> B", "A <=> B", Runner.parse("(a and a) <=> B".toUpperCase()))
  }

  @Test def testNr45() {
    assertEquals("Powinno byc: A => B", "A => B", Runner.parse("A => B".toUpperCase()))
  }

  @Test def testNr46() {
    assertEquals("Powinno byc: (E OR C)", "(E OR C) <=> NOT (A OR F)", Runner.parse("(((not d or not e) and ((not a and a) and ((not true) or b))) or ((not not true and e) or c)) <=> (NOT A AND NOT F)".toUpperCase()))
  }
}